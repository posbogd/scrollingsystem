﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;


public class ScrollViewManagerLeft : MonoBehaviour {
    [Range(1f,10f)]
    public float scaler;

    public GameObject shiny;
    public float moveToCenterSpeed;

    public RectTransform center;
    private RectTransform content;
    private RectTransform[] items; 

    private float deltaLocalPosition; //local position ofset

    int itemsLength;  //items[itemLength]
    float itemWidth; //item width
    float spacing;   //spacing width
   // public float scaledValue;
    private int middleItem; //temp value
    private float midleItemValue; 

    void Start ()
    {
        shiny.gameObject.SetActive(true);
        itemWidth = 450f;
        spacing = 50f;
        itemsLength = 20;
      //  scaledValue = 585f;

        content = GetComponent<RectTransform>();
        items = new RectTransform[itemsLength];

        deltaLocalPosition = content.localPosition.x;
        for (int i = 0; i < items.Length; i++)
        {
            items[i] = GameObject.Find("/Canvas/ScrollView/Content/Image" + i).GetComponent<RectTransform>();
        }
    }


    #region ScrollView events and buttons functions

    public void OnInitPotentialDrag()
    {
        shiny.gameObject.SetActive(false);
        middleItem = SelectedItemID();
        StopAllCoroutines();
    }

    public void OnDragSetObjects()
    {
       // ItemsScaler();

        if (middleItem != SelectedItemID())
        {
            if ((middleItem - SelectedItemID() == 1))
            {
                IfScrollLeft(middleItem);
                
            }
            else
            {
                IfScrollRight(middleItem);
                
            }

            middleItem = SelectedItemID();
        }
        
    }
    public void OnEndDrag()
    {
        // content.localPosition = new Vector2(deltaLocalPosition + PositionToID(content.localPosition.x) * (itemWidth+spacing), 0f);

       StartCoroutine(ItemMovementCR(new Vector2(deltaLocalPosition + PositionToID(content.localPosition.x) * (itemWidth + spacing), 0f)));

        
    }
    public void TestBT()
    {
        //  Debug.Log(PositionToID(content.localPosition.x ));
        // content.localPosition = new Vector2(deltaLocalPosition + PositionToID(content.localPosition.x) * itemWidth, 0f);
        //Debug.Log(content.localPosition.x);


        Debug.Log(1 - Vector3.Distance(center.position, items[SelectedItemID() - 1].position) / 800);
        
        Debug.Log(1 - Vector3.Distance(center.position, items[SelectedItemID()].position) / 800);
        Debug.Log(1 - Vector3.Distance(center.position, items[SelectedItemID() + 1].position) / 800);

        // Debug.Log(Vector3.Distance(items[SelectedItemID()].localPosition, items[SelectedItemID() - 1].localPosition));
        // Debug.Log(Vector3.Distance(items[SelectedItemID()].localPosition, items[SelectedItemID() + 1].localPosition));

    }
    #endregion

    #region Maths functions



    private void IfScrollLeft(int middleItem)
    {   //----------
      //  items[middleItem].sizeDelta = new Vector2(itemWidth, itemWidth);
        SetOpacity(middleItem, 128);
        //------------
        if (IsInArray(middleItem - 1))
        {
          //  items[middleItem - 1].sizeDelta = new Vector2(scaledValue, scaledValue);
          //  SetOpacity(middleItem-1, 255);
        }
        if (IsInArray(middleItem - 2))
        {
            items[middleItem - 2].gameObject.SetActive(true);
           // SetOpacity(middleItem - 2, 128);
        }
        if (IsInArray(middleItem + 1))
        {
            items[middleItem + 1].gameObject.SetActive(false);
           //SetOpacity(middleItem + 1, 255);
        }
    }
    private void IfScrollRight(int middleItem)
    {   //----------
       // items[middleItem].sizeDelta = new Vector2(itemWidth, itemWidth);
      //  SetOpacity(middleItem, 128);
        //------------
        if (IsInArray(middleItem + 1))
        {
          //  items[middleItem + 1].sizeDelta = new Vector2(scaledValue, scaledValue);
         //   SetOpacity(middleItem + 1, 255);
        }
        if (IsInArray(middleItem + 2))
        {
            items[middleItem + 2].gameObject.SetActive(true);
          //  SetOpacity(middleItem + 2, 128);
        }
        if (IsInArray(middleItem - 1))
        {
            items[middleItem - 1].gameObject.SetActive(false);
        //    SetOpacity(middleItem - 1, 255);
        }
    }


    //Convert position float value to Int array -index
    private int PositionToID(float contentPositionX)
    {
        return (int)Math.Round((contentPositionX - deltaLocalPosition) / (itemWidth+spacing));
    }

    private bool IsInArray(int element)
    {
        if ((element >= 0) && (element < itemsLength))
            return true;
        else
            return false;
    }
    private int SelectedItemID()
    {
        return -1 * (PositionToID(content.localPosition.x));
    }
    #endregion

    private void SetOpacity(int itemID, byte oppacity)
    {
        items[itemID].GetComponent<Image>().color = new Color32(255,255,255,oppacity);
    }


    IEnumerator ItemMovementCR(Vector3 destinationPoint)
    {
      

        while (Vector3.Distance(content.localPosition, destinationPoint) > 0.1f)
        {
            // content.localPosition = new Vector2(deltaLocalPosition + PositionToID(content.localPosition.x) * (itemWidth + spacing), 0f          
            // content.localPosition= destinationPoint;
               content.localPosition = Vector3.Lerp(content.localPosition, destinationPoint, moveToCenterSpeed * Time.deltaTime);
             
            
            yield return null;
        }
        shiny.gameObject.SetActive(true);

    }

    private void ItemsScaler()
    {
        if (IsInArray(SelectedItemID() - 1))
        {
            items[SelectedItemID() - 1].localScale = new Vector3(0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID() - 1].position) / 800) / scaler, 0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID() - 1].position) / 800) / scaler, 1f);
            SetOpacity(middleItem - 1, (byte)((0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID() - 1].position) / 800) * 255) ));
        }
        items[SelectedItemID()].localScale = new Vector3(0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID()].position) / 800) / scaler, 0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID()].position) / 800) / scaler, 1f);
        SetOpacity(middleItem, (byte)((0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID()].position) / 800) * 255) ));
    
        if (IsInArray(SelectedItemID() + 1))
        {
            items[SelectedItemID() + 1].localScale = new Vector3(0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID() + 1].position) / 800) / scaler, 0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID() + 1].position) / 800) / scaler, 1f);
            SetOpacity(middleItem + 1, (byte)((0.9f + (1 - Vector3.Distance(center.position, items[SelectedItemID() + 1].position) / 800) * 255) ));
        }
    }

    private void Update()
    {
        ItemsScaler();
    }
}
